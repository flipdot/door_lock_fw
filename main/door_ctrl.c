#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <freertos/event_groups.h>
#include "freertos/portmacro.h"
#include <esp_log.h>
#include <string.h>
#include <driver/uart.h>
#include <driver/adc.h>
#include <driver/spi_master.h>
#include <esp_adc_cal.h>
#include <driver/mcpwm.h>
#include "door_ctrl.h"
#include "led.h"
#include "tmc2130.h"

static const char* TAG = "DOOR_CTRL";
static const int MOTOR_DELAY_MS = 50;
static const int MOTOR_TIMEOUT_MS = 3000;

static const int MOTOR_TIMEOUT = MOTOR_TIMEOUT_MS/MOTOR_DELAY_MS;

static const gpio_num_t DOOR_CTRL_EN = GPIO_NUM_13;
static const gpio_num_t DOOR_CTRL_DIR = GPIO_NUM_4;
static const gpio_num_t DOOR_CTRL_STEP = GPIO_NUM_5;
static const gpio_num_t DOOR_CTRL_CLOSEBTN_PIN = GPIO_NUM_17;
static const gpio_num_t DOOR_CTRL_OPENBTN_PIN = GPIO_NUM_36;
static const gpio_num_t DOOR_CTRL_SUMMER_PIN = GPIO_NUM_14;
static const adc1_channel_t DOOR_POT_ADC_CHAN = ADC1_CHANNEL_3;


static const bool DOOR_TMC_GIO_ENABLE = 0;
static const bool DOOR_TMC_GIO_DISABLE = 1;

static const int pot_open = 1900;
static const int pot_close = 800;
static const int pot_mid = pot_open > pot_close ? (pot_open - pot_close)/2 + pot_close : (pot_close - pot_open)/2+pot_open;

QueueHandle_t sem;
QueueHandle_t door_ctrl_queue;
static QueueHandle_t door_btn_intr_queue;

extern xQueueHandle led_queue_handle;
extern EventGroupHandle_t door_state_event_group;


static const int NO_OF_SAMPLES = 10;
static esp_adc_cal_characteristics_t *adc_chars;

static led_state_t led;

//#define sim
#if defined(sim)
static int my_sim = 200;
static int8_t sim_dir = -50;
#endif

static uint32_t door_ctr_get_pot(){
    uint32_t adc_reading = 0;
    //Multisampling
    for (int i = 0; i < NO_OF_SAMPLES; i++) {
        adc_reading += adc1_get_raw((adc1_channel_t)DOOR_POT_ADC_CHAN);
    }
    adc_reading /= NO_OF_SAMPLES;
    //Convert adc_reading to voltage in mV
    int32_t voltage = esp_adc_cal_raw_to_voltage(adc_reading, adc_chars);
#if defined(sim)
    my_sim += sim_dir;
    if(my_sim > pot_close || my_sim < pot_open){
        sim_dir *= -1;
    }
    voltage = my_sim;
#endif
    int32_t p = ((voltage-pot_open)*100)/(pot_close-pot_open);
    if(p > 100) p = 100;
    if(p < 0) p = 0;
    ESP_LOGI(TAG, "adc %dmV l = %d", voltage, p);
    led.led_id = LED_DOOR_STATE;
    led.color.rgb.r = p*0.75;
    led.color.rgb.g = (100-p)*0.75;
    led.color.rgb.b = 0;
    xQueueSend(led_queue_handle, &led, 10/portTICK_PERIOD_MS);
    if(voltage > pot_mid){
        xEventGroupSetBits(door_state_event_group, 1);
    } else{
        xEventGroupClearBits(door_state_event_group, 1);
    }
    return voltage;
}

static void door_ctrl_motor_en(uint8_t dir){
    tmc_enable();
    vTaskDelay(100/portTICK_PERIOD_MS);
    gpio_set_level(DOOR_CTRL_DIR, dir);
    gpio_set_level(DOOR_CTRL_EN, DOOR_TMC_GIO_ENABLE);
    vTaskDelay(100/portTICK_PERIOD_MS);
    mcpwm_start(MCPWM_UNIT_0, MCPWM_TIMER_0);
}

static void door_ctrl_motor_dis(void){

    mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);
    gpio_set_level(DOOR_CTRL_EN, DOOR_TMC_GIO_DISABLE);

    tmc_disable();
}

static void door_ctrl_open_door(){
    uint32_t count = 0;
    ESP_LOGI(TAG, "open");
    door_ctrl_motor_en(1);
    while(door_ctr_get_pot() < pot_open){
        vTaskDelay(MOTOR_DELAY_MS/portTICK_PERIOD_MS);
        if(count++ > MOTOR_TIMEOUT){
            ESP_LOGE(TAG, "Motor timeout while opening");
            break;
        }
    }
    door_ctrl_motor_dis();
}

static void door_ctrl_close_door(){
    ESP_LOGI(TAG, "close");
    uint32_t count = 0;
    door_ctrl_motor_en(0);
    while(door_ctr_get_pot() > pot_close){
        vTaskDelay(MOTOR_DELAY_MS/portTICK_PERIOD_MS);
        if(count++ > MOTOR_TIMEOUT){
            ESP_LOGE(TAG, "Motor timeout while closing");
            break;
        }
    }
    door_ctrl_motor_dis();
}

static void door_ctrl_summer(){
    ESP_LOGI(TAG, "summ");
    gpio_set_level(DOOR_CTRL_SUMMER_PIN, 1);
    vTaskDelay(1000/portTICK_PERIOD_MS);
    gpio_set_level(DOOR_CTRL_SUMMER_PIN, 0);
}


static void door_ctrl_maintask(void *p){
    door_ctrl_cmd_t door_ctrl_cmd;
    adc1_config_width(ADC_WIDTH_BIT_12);
    adc1_config_channel_atten(DOOR_POT_ADC_CHAN,ADC_ATTEN_DB_6);
    adc_chars = calloc(1, sizeof(esp_adc_cal_characteristics_t));
    esp_adc_cal_value_t val_type = esp_adc_cal_characterize(ADC_UNIT_1, ADC_ATTEN_DB_6, ADC_WIDTH_BIT_12, 1100, adc_chars);
    while (1){
        if(pdPASS == xQueueReceive(door_ctrl_queue, &door_ctrl_cmd, 1000/portTICK_PERIOD_MS)){
            switch(door_ctrl_cmd){
                case DOOR_CTRL_CMD_OPEN:
                    door_ctrl_open_door();
                    break;
                case DOOR_CTRL_CMD_CLOSE:
                    door_ctrl_close_door();
                    break;
                case DOOR_CTRL_CMD_SUMMER:
                    door_ctrl_summer();
                    break;
                case DOOR_CTRL_CMD_GET_STATE:

                default:
                    ESP_LOGE(TAG, "unknown door_ctrl_cmd %d", door_ctrl_cmd);
            }
        }
        door_ctr_get_pot();
    }
}


static void door_ctrl_btn_task(void *p){
   while(1){
        door_ctrl_cmd_t cmd = DOOR_CTRL_CMD_CLOSE;
        if(gpio_get_level(DOOR_CTRL_OPENBTN_PIN) == 1){
           cmd = DOOR_CTRL_CMD_OPEN;
           xQueueSend(door_ctrl_queue, &cmd, 1000 / portTICK_PERIOD_MS);
           //debounce
           vTaskDelay(3000/portTICK_PERIOD_MS);
        }
        else if(gpio_get_level(DOOR_CTRL_CLOSEBTN_PIN) == 0){
           xQueueSend(door_ctrl_queue, &cmd, 1000 / portTICK_PERIOD_MS);
           //debounce
           vTaskDelay(3000/portTICK_PERIOD_MS);
        }else{
            vTaskDelay(200/portTICK_PERIOD_MS);
        }
   }
}

esp_err_t door_ctrl_init(){
    gpio_config_t io_conf;
    io_conf.intr_type = GPIO_PIN_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = ((1ULL << DOOR_CTRL_DIR) | (1ULL << DOOR_CTRL_EN ));
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 1;
    gpio_set_level(DOOR_CTRL_DIR, 0);
    gpio_set_level(DOOR_CTRL_EN, 1);
    gpio_config(&io_conf);
    gpio_set_level(DOOR_CTRL_DIR, 0);
    gpio_set_level(DOOR_CTRL_EN, 1);
    io_conf.pin_bit_mask = ((1ULL << DOOR_CTRL_SUMMER_PIN));
    io_conf.pull_down_en = 1;
    io_conf.pull_up_en = 0;
    gpio_set_level(DOOR_CTRL_SUMMER_PIN, 0);
    gpio_config(&io_conf);
    gpio_set_level(DOOR_CTRL_SUMMER_PIN, 0);


    mcpwm_gpio_init(MCPWM_UNIT_0, MCPWM0A, DOOR_CTRL_STEP);

    mcpwm_config_t pwm_config = {
        .frequency = 100000, // frequency = 100kHz
        .cmpr_a = 50,     // duty cycle [%]
        .counter_mode = MCPWM_UP_COUNTER,
        .duty_mode = MCPWM_DUTY_MODE_0,
    };
    mcpwm_init(MCPWM_UNIT_0, MCPWM_TIMER_0, &pwm_config);
    mcpwm_stop(MCPWM_UNIT_0, MCPWM_TIMER_0);


    /* Configure close button intr */
    gpio_set_direction(DOOR_CTRL_CLOSEBTN_PIN, GPIO_MODE_INPUT);
    gpio_set_direction(DOOR_CTRL_OPENBTN_PIN, GPIO_MODE_INPUT);
    gpio_pullup_dis(DOOR_CTRL_OPENBTN_PIN);
    gpio_pulldown_en(DOOR_CTRL_OPENBTN_PIN);

    gpio_pulldown_dis(DOOR_CTRL_CLOSEBTN_PIN);
    gpio_pullup_en(DOOR_CTRL_CLOSEBTN_PIN);

    gpio_set_intr_type(DOOR_CTRL_CLOSEBTN_PIN, GPIO_INTR_NEGEDGE);
    gpio_set_intr_type(DOOR_CTRL_OPENBTN_PIN, GPIO_INTR_POSEDGE);

    door_btn_intr_queue = xQueueCreate(10, sizeof(uint32_t));
    if (NULL == door_btn_intr_queue) {
        ESP_LOGE(TAG, "Failed to create door_btn_intr_queue");
        return ESP_FAIL;
    }
    //sem = xSemaphoreCreateBinary();
    //gpio_install_isr_service(0);
    //gpio_isr_handler_add(DOOR_CTRL_BTN_PIN, door_ctrl_btn_isr, (void*)DOOR_CTRL_BTN_PIN);

    door_ctrl_queue = xQueueCreate(2, sizeof(door_ctrl_cmd_t));
    if (NULL == door_ctrl_queue){
        ESP_LOGE(TAG, "Failed to create door_ctrl queue");
        return ESP_FAIL;
    }
    if(pdPASS != xTaskCreate(door_ctrl_maintask, "door_ctrl", 2048, NULL, 0, NULL)){
        ESP_LOGE(TAG, "Failed to create door_ctrl task");
        return ESP_FAIL;
    }
    if(pdPASS != xTaskCreate(door_ctrl_btn_task, "door_ctrl_btn", 1024, NULL, 0, NULL)){
        ESP_LOGE(TAG, "Failed to create door_ctrl_btn task");
        return ESP_FAIL;
    }
    return ESP_OK;
}


