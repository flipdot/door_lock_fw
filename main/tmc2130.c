#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include <esp_log.h>
#include <driver/spi_master.h>
#include <driver/gpio.h>

static const char* TAG = "TMC2130";

static spi_device_handle_t spi_stepper_handle;

static const gpio_num_t DOOR_SPI_CS = GPIO_NUM_23;
static const gpio_num_t DOOR_SPI_MOSI = GPIO_NUM_21;
static const gpio_num_t DOOR_SPI_MISO = GPIO_NUM_19;
static const gpio_num_t DOOR_SPI_SCK = GPIO_NUM_22;


typedef enum{
    TMC_REG_GCONF       = 0x00,
    TMC_REG_GSTAT       = 0x01,
    TMC_REG_IOIN        = 0x04,
    TMC_REG_IHOLD_IRUN  = 0x10,
    TMC_REG_TPOWER_DOWN = 0x11,
    TMC_REG_TSTEP       = 0x12,
    TMC_REG_XDIRECT     = 0x2D,
    TMC_REG_CHOPCONF    = 0x6C,
    TMC_REG_COOLCONF    = 0x6D,
    TMC_REG_DRV_STATUS  = 0x6F,
    TMC_REG_PWMCONF     = 0x70,
} TMC_REGISTER;







static uint32_t tmc_spi_trans(TMC_REGISTER reg, uint32_t value){
    esp_err_t ret;

    struct _req {
        uint8_t cmd;
        uint32_t data;
    } __attribute__ ((packed))  tx, rx;
    spi_transaction_t trans;
    trans.addr = 0;
    trans.rxlength = sizeof(rx)*8;
    trans.length = sizeof(tx)*8;
    trans.rx_buffer = &rx;
    trans.tx_buffer = &tx;
    tx.cmd = reg;
    tx.data = SPI_SWAP_DATA_TX(value, 32);

    rx.cmd = 0;
    rx.data = 0;
    //trans.flags = SPI_TRANS_USE_RXDATA | SPI_TRANS_USE_TXDATA;
    trans.flags = 0;
    ret = spi_device_polling_transmit(spi_stepper_handle, &trans);
    ESP_ERROR_CHECK(ret);
    rx.data = SPI_SWAP_DATA_RX(rx.data, 32);

    ESP_LOGI(TAG, "stepper reg %02X =  %08X s= %02X", reg, rx.data, rx.cmd);

    return 0;
}

static uint32_t tmc_read_reg(TMC_REGISTER reg){
    return tmc_spi_trans(reg, 0);
}

static uint32_t tmc_write_reg(TMC_REGISTER reg, uint32_t value){
    return tmc_spi_trans(reg | 0x80, value);
}

static esp_err_t tmc_spi_init(void){
    gpio_pulldown_en(DOOR_SPI_MOSI);
    esp_err_t ret;
        spi_bus_config_t buscfg={
        .miso_io_num = DOOR_SPI_MISO,
        .mosi_io_num = DOOR_SPI_MOSI,
        .sclk_io_num = DOOR_SPI_SCK,
        .quadwp_io_num = -1,
        .quadhd_io_num = -1,
        .max_transfer_sz = 64,
    };

    //Initialize the SPI bus
    ret = spi_bus_initialize(SPI2_HOST, &buscfg, 2);
    ESP_ERROR_CHECK(ret);

    spi_device_interface_config_t devcfg={
        .command_bits = 0,
        .address_bits = 0,
        .clock_speed_hz=2*1000*1000,           //Clock out at 10 MHz
        .mode=3,                                //SPI mode 0
        .spics_io_num=DOOR_SPI_CS,               //CS pin
        .queue_size=7,                          //We want to be able to queue 7 transactions at a time
        .pre_cb=NULL,
    };
    ret = spi_bus_add_device(SPI2_HOST, &devcfg, &spi_stepper_handle);
    ESP_ERROR_CHECK(ret);

    return ret;
}


esp_err_t tmc_enable(void){
    return tmc_write_reg(TMC_REG_GCONF, 4);
}


esp_err_t tmc_disable(void){
    return tmc_write_reg(TMC_REG_GCONF, 0);
}

esp_err_t tmc2130_init(void){
    tmc_spi_init();
    tmc_write_reg(TMC_REG_GCONF, 0);
    tmc_write_reg(TMC_REG_CHOPCONF, 0);
    tmc_write_reg(TMC_REG_COOLCONF, 0);
    tmc_write_reg(TMC_REG_PWMCONF, 0x50480);
    tmc_write_reg(TMC_REG_IHOLD_IRUN, 0);
    tmc_write_reg(TMC_REG_CHOPCONF, 0x8008);

    tmc_write_reg(TMC_REG_IHOLD_IRUN, 31UL<< 8);
    tmc_read_reg(TMC_REG_GCONF);

//    while(1){
//        //tmc_read_reg(TMC_REG_IOIN);
//        //vTaskDelay(100/portTICK_PERIOD_MS);
////        tmc_write_reg(TMC_REG_IHOLD_IRUN, 0xAABBCCDD);
//        tmc_read_reg(TMC_REG_DRV_STATUS);
//        vTaskDelay(1000/portTICK_PERIOD_MS);
//        //tmc_read_reg(TMC_REG_XDIRECT);
//        //vTaskDelay(100/portTICK_PERIOD_MS);
//        //tmc_read_reg(TMC_REG_GSTAT);
//        //vTaskDelay(2000/portTICK_PERIOD_MS);
//    }


    return ESP_OK;
}
