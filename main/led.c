#include <freertos/FreeRTOS.h>
#include <freertos/task.h>
#include "esp_err.h"
#include <driver/rmt.h>
#include <driver/gpio.h>
#include <esp_log.h>

#include "led.h"

static const char* TAG = "LED";

static led_state_t leds[LED_NUM_LEDS] = {};

xQueueHandle led_queue_handle = NULL;

static void led_generate_rmt_item(rmt_item32_t *data){
    for(uint8_t i = 0; i < LED_NUM_LEDS; i++){
        for(uint8_t c = 0; c < 3; c++){
            uint8_t cur_byte = leds[i].color.bytes[c];
            //for(uint8_t b = 0; b < 8; b++){
            uint8_t b = 8;
            while(b){
                b-=1;
                rmt_item32_t* cur = &data[i*3*8+c*8+7-b];
                if(cur_byte & (1<<b)){
                    cur->duration0 = 8;
                    cur->level0 = 1;
                    cur->duration1 = 5;
                    cur->level1 = 0;
                }else{
                    cur->duration0 = 4;
                    cur->level0 = 1;
                    cur->duration1 = 9;
                    cur->level1 = 0;

                }
            }
        }
    }

}



void led_maintask(void *pv){
    rmt_item32_t items[LED_NUM_LEDS*3*8];

    led_state_t state;
    while(true){
        if(xQueueReceive(led_queue_handle, &state, 100/portTICK_PERIOD_MS)){
            leds[state.led_id] = state;
            led_generate_rmt_item(items);
        }
#if 0
        for(uint8_t i = 0; i < LED_NUM_LEDS; i++){
            leds[i].color.bytes[0] = 0;
            leds[i].color.bytes[1] = 0;
            leds[i].color.bytes[2] = 0;
        }
        led_generate_rmt_item(items);
#endif

        rmt_write_items(RMT_CHANNEL_0, items, LED_NUM_LEDS*3*8, true);

    }
    vTaskDelete(NULL);
}


esp_err_t led_init(void){
    rmt_config_t rmt_cfg = {
        .rmt_mode = RMT_MODE_TX,
        .channel = RMT_CHANNEL_0,
        .clk_div = 8,
        .gpio_num = GPIO_NUM_12,
        .mem_block_num = 1,
        .tx_config = {
            .loop_en = false,
            .carrier_freq_hz = 42, // Unused, Div by zero if 0
            .carrier_duty_percent = 50,
            .carrier_level = RMT_CARRIER_LEVEL_LOW,
            .carrier_en = false,
            .idle_level = RMT_IDLE_LEVEL_LOW,
            .idle_output_en = true,
        }
    };

    esp_err_t cfg_ok = rmt_config(&rmt_cfg);
    if (cfg_ok != ESP_OK) {
        return false;
    }
    esp_err_t install_ok = rmt_driver_install(rmt_cfg.channel, 0, 0);
    if (install_ok != ESP_OK) {
        return false;
    }

    led_queue_handle = xQueueCreate(2, sizeof(led_state_t));
    if(NULL == led_queue_handle){
        ESP_LOGE(TAG, "failed to create LED queue");
        return ESP_FAIL;
    }

    if(pdFAIL == xTaskCreate(led_maintask, "led", 1024, NULL, 0, NULL)){
        ESP_LOGE(TAG, "failed to create led task");
        return ESP_FAIL;
    }

    //led_state_t init_state;
    //init_state.led_id = LED_WIFI;
    //init_state.color.rgb.b = 100;
    //init_state.color.rgb.r = 0;
    //init_state.color.rgb.g = 0;
    //xQueueSend(led_queue_handle, &init_state, 100/portTICK_PERIOD_MS);
    //init_state.led_id = LED_ERROR;
    //init_state.color.rgb.b = 0;
    //init_state.color.rgb.r = 100;
    //init_state.color.rgb.g = 0;

    //xQueueSend(led_queue_handle, &init_state, 100/portTICK_PERIOD_MS);

    return ESP_OK;
}
