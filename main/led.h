#include <freertos/FreeRTOS.h>


enum {
    LED_WIFI = 0,
    LED_DOOR_STATE,
    LED_ERROR,

    LED_NUM_LEDS, // This is not an LED
} LED_ID;

typedef union __color_t {
    struct __rgb {
        uint8_t g;
        uint8_t r;
        uint8_t b;
    } rgb;
    uint8_t bytes[3];
} color_t;

typedef struct _led{
    enum _State {
        LED_STATE_ON,
        LED_STATE_OFF,
        LED_STATE_FLASH
    } led_state;
    uint8_t led_id;
    color_t color;
} led_state_t;

esp_err_t led_init(void);

