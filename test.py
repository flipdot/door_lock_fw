import serial



s = serial.Serial("/dev/ttyUSB0", baudrate=9600)

s.write(b"\x022C0045C12080")
s.close()


import smbus
import time
bus = smbus.SMBus(1)

address = 0x42
cmd = 0x23

def writeNumber(value):
    bus.write_byte(address, value)
    return -1

def readNumber():
    number = bus.read_byte(address)
    return number

while True:
    bus.write_word_data(address, cmd, 0xCAFE)
    time.sleep(3)

